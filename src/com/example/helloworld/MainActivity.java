package com.example.helloworld;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	public int i=0; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void buttonClicked(View view) {
		if (i==0) {
			view.setBackgroundColor(Color.BLUE);
			Button p1_button = (Button)findViewById(R.id.button1);
			p1_button.setText("Click Again!");
			TextView tv = (TextView)findViewById(R.id.textView1); 
			tv.setText("You just pressed a button!"); 
			i=1; 
		}
		
		else if (i==1){
			view.setBackgroundColor(Color.GRAY);  
			Button p1_button = (Button)findViewById(R.id.button1);
			p1_button.setText("Click me!");
			TextView tv = (TextView)findViewById(R.id.textView1); 
			tv.setText("Adnan Tahir"); 
			i=0; 
		}
	}

}

