1. Hello World Application screenshot:
   		http://imgur.com/YqVt6hQ

2. Interactive Application 

	This is an interactive android application for testing.This is the homescreen of the app
		http://imgur.com/a0XbOI9

	Pressing the "Click me!" button, changes the color of the button to blue, and changes the text to "You just pressed a button". 
	Clicking Again reverses the operation. 
		http://imgur.com/hgSfrzc
		
3. bitbucket link
	https://bitbucket.org/adnant/eecs481-helloworld
	
All images are in also in the 'imagesOfApplication' folder